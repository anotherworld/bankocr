Assumptions / Simplifications / Notes
=====================================


User Story 1
------------

DigitalAccountNumberParser:
- \r\n - for quickness - should probably use Environment.NewLine or similar
- FormattedAccountNumberLength const could be AccountNumberLength * DigitWidth - but used 27 to relate to the instructions
- GetIndividualDigits could work out numberOfDigits: length of line(s) / DigitWidth. Omitted for simplicity as additional logic and checks would be needed
- DigitalAccountNumberParser could be split into two classes - e.g. DigitParser and AccountNumberParser, taking a dependency on say IDigitParser (as demonstrated by AccountNumberFileProcessor, taking a depdency on IAccountNumberParser)
- No DI is used here - in a production system, you may want to create a DI container to establish relevant concrete types to instantiate instead of creating manually (i.e. in Program.cs)
- Could go a step further with the OO and have models/objects for AccountNumber / AccountNumberFormatted (or AccountNumberEntry) - but not much benefit at this level of complexity

AccountNumberBatchProcessor:
- EntryLineCount instead of using a constant, could probably better make use of the dependent IAccountNumberParser's DigitHeight value (though would need some refactoring)


User Story 2
------------

AccountNumberValidator
 - Moved error message prefixes into constants to de-magic string the error messages somewhat across production code and tests


User Story 3
------------

- Refactoring - ParseAccountNumber to return an AccountNumberParseResult instead of string. Can be applied across the board to return objects with statuses/errors instead of throwing exceptions. e.g. DigitParseResult + incorporate Validation and Errors list in the result objects
- AccountNumberParseResult renamed to DigitsParseResult (and its members too accordingly)
- An alternative design in hindsight might be to use recursion to deal with parsing single/multiple digits - to reduce some code duplication


User Story 4
------------

To be discussed - general approach:
- we have a collection of parse results, the original pipes & underscores formatted number, and the parsed account numbers
- write a unit test to assert given erroneous account number that can be remedied by a pipe or underscore fix, returns a valid account number
- write unit test to check for ParseStatus.AMB when we review the erroenous results, and assert that specified inputs results in a range of valid alternatives
- in either scenario, by means of a new service (and/or augmenting the parser with a new dependency) (e.g. AccountNumberParserErrorCorrector) select the ones that are of ParseStatus.ERR or ParseStatus.ILL
- add a PossibleValues property to ParseResult in case of AMB
- in the error correction process, for each erroneous account number, generate a list of alternatives by:
 - adding a pipe, then underscore, alternately to each digit one by one, where there is a space, calculate the checksum - if valid, add to the list of alternatives
 - if one alternative, parse result should be OK and the valid number returned
 - if more than one alternative, parse result should be AMB


General Notes
-------------

- With extra time / also in hindsight I'd have maybe split out the account number concept, its parsing and the validation a bit cleaner
- Could do with a bit of gold plating/tidying for better consistency

Program:
- Could use a Builder pattern for a more fluent, maybe readable instantiation. e.g. AccountNumberBatchProcessorBuilder.FileBasedBatchProcessor().UsingDigitalAccountNumberParser().Build();
- A step further might be to have another class for outputting the various parse/validation results, and using an ILogger / ITextOutput or similar instead of Console.WriteLine to abstract the UI aspect (if necessary)

