using System;
using System.Linq;
using BankOCR.Console.Interfaces;
using BankOCR.Console.Models;
using BankOCR.Console.Services;
using Moq;
using Xunit;

namespace BankOCR.Tests
{
    public class AccountNumberBatchProcessorTests
    {
        [Fact]
        public void ProcessFile_ValidFormat_ReturnsAccountNumbers()
        {
            var accountNumberParserMock = new Mock<IAccountNumberParser>();
            accountNumberParserMock
                .SetupSequence(x => x.ParseAccountNumber(It.IsAny<string>()))
                .Returns(new DigitsParseResult("", "123456789", ParseStatus.OK))
                .Returns(new DigitsParseResult("", "678912354", ParseStatus.OK))
                .Returns(new DigitsParseResult("", "456789123", ParseStatus.OK))
                .Returns(new DigitsParseResult("", "120456789", ParseStatus.OK))
                .Returns(new DigitsParseResult("", "670912354", ParseStatus.OK))
                .Returns(new DigitsParseResult("", "456780123", ParseStatus.OK));

            IAccountNumberFileProcessor processor = new AccountNumberBatchProcessor(accountNumberParserMock.Object);

            const string inputs = "    _  _     _  _  _  _  _ \r\n" +
                                  "  | _| _||_||_ |_   ||_||_|\r\n" +
                                  "  ||_  _|  | _||_|  ||_| _|\r\n" +
                                  "\r\n" +
                                  " _  _  _  _     _  _     _ \r\n" +
                                  "|_   ||_||_|  | _| _||_||_ \r\n" +
                                  "|_|  ||_| _|  ||_  _|  | _|\r\n" +
                                  "\r\n" +
                                  "    _  _  _  _  _     _  _ \r\n" +
                                  "|_||_ |_   ||_||_|  | _| _|\r\n" +
                                  "  | _||_|  ||_| _|  ||_  _|\r\n" +
                                  "\r\n" +
                                  "    _  _     _  _  _  _  _ \r\n" +
                                  "  | _|| ||_||_ |_   ||_||_|\r\n" +
                                  "  ||_ |_|  | _||_|  ||_| _|\r\n" +
                                  "\r\n" +
                                  " _  _  _  _    _  _      _ \r\n" +
                                  "|_   || ||_|  | _| _||_||_ \r\n" +
                                  "|_|  ||_| _|  ||_  _|  | _|\r\n" +
                                  "\r\n" +
                                  "    _  _  _  _  _     _  _ \r\n" +
                                  "|_||_ |_   ||_|| |  | _| _|\r\n" +
                                  "  | _||_|  ||_||_|  ||_  _|\r\n";

            var lines = inputs.Split("\r\n");
            var accountNumbers = processor.ProcessLines(lines);

            accountNumberParserMock.Verify(x => x.ParseAccountNumber(It.IsAny<string>()));
            Assert.Equal(new[] {"123456789", "678912354", "456789123", "120456789", "670912354", "456780123"}, accountNumbers.Select(x => x.DigitsParsed).ToArray());
        }

        [Fact]
        public void ProcessFile_InvalidArgumentNonMultipleOfEntrySize_ThrowsException()
        {
            IAccountNumberFileProcessor processor = new AccountNumberBatchProcessor(Mock.Of<IAccountNumberParser>());

            const string inputs = "\r\n" +
                                  "\r\n";

            var lines = inputs.Split("\r\n");
            var action = new Action(() => processor.ProcessLines(lines));

            var ex = Assert.Throws<ArgumentException>(action);
            Assert.Contains("Incorrect line count", ex.Message);
        }

        [Fact]
        public void ProcessFile_InvalidFormatIncorrectEntrySpacing_ThrowsException()
        {
            IAccountNumberFileProcessor processor = new AccountNumberBatchProcessor(Mock.Of<IAccountNumberParser>());

            const string inputs = "    _  _     _  _  _  _  _ \r\n" +
                                  "  | _| _||_||_ |_   ||_||_|\r\n" +
                                  "  ||_  _|  | _||_|  ||_| _|\r\n" +
                                  "\r\n" +
                                  " _  _  _  _     _  _     _ \r\n" +
                                  "|_   ||_||_|  | _| _||_||_ \r\n" +
                                  "|_|  ||_| _|  ||_  _|  | _|\r\n" +
                                  "    _  _  _  _  _     _  _ \r\n" +
                                  "|_||_ |_   ||_||_|  | _| _|" +
                                  "  | _||_|  ||_| _|  ||_  _|\r\n" + 
                                  "\r\n" +
                                  "\r\n";
;
            var lines = inputs.Split("\r\n");
            var action = new Action(() => processor.ProcessLines(lines));

            var ex = Assert.Throws<FormatException>(action);
            Assert.Contains("Invalid format", ex.Message);
        }
    }
}
