using System.ComponentModel.DataAnnotations;
using System.Linq;
using BankOCR.Console.Interfaces;
using BankOCR.Console.Services;
using BankOCR.Console.Validation;
using Moq;
using Xunit;

namespace BankOCR.Tests
{
    public class AccountNumberValidatorTests
    {
        [Theory]
        [InlineData("7111111111")]
        [InlineData("12345678")]
        [InlineData("")]
        public void Validate_IncorrectLength_ShouldReturnIncorrectLengthError(string accountNumber)
        {
            var validator = new AccountNumberValidator();

            var result = validator.Validate(accountNumber);

            Assert.Contains(result.Errors, x => x.StartsWith(AccountNumberValidator.IncorrectLengthPrefix));
        }

        [Theory]
        [InlineData("711111111")]
        [InlineData("123456789")]
        [InlineData("490867715")]
        public void Validate_ValidAccountNumber_ShouldReturnNoErrors(string accountNumber)
        {
            var validator = new AccountNumberValidator();

            var result = validator.Validate(accountNumber);

            Assert.True(!result.Errors.Any());
        }

        [Theory]
        [InlineData("888888888")]
        [InlineData("490067715")]
        [InlineData("012345678")]
        public void Validate_ValidAccountNumber_ShouldReturnChecksumError(string accountNumber)
        {
            var validator = new AccountNumberValidator();

            var result = validator.Validate(accountNumber);

            Assert.Contains(result.Errors, x => x.StartsWith(AccountNumberValidator.ChecksumErrorPrefix));
        }
    }
}
