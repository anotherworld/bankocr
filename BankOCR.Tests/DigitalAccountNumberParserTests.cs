using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BankOCR.Console.Interfaces;
using BankOCR.Console.Models;
using BankOCR.Console.Services;
using BankOCR.Console.Validation;
using Moq;
using Xunit;
using ValidationResult = BankOCR.Console.Models.ValidationResult;

namespace BankOCR.Tests
{
    public class DigitalAccountNumberParserTests
    {
        [Theory]
        [InlineData(" _ " +
                    "| |" +
                    "|_|", "0")]
        [InlineData("   " +
                    "  |" +
                    "  |", "1")]
        [InlineData(" _ " +
                    " _|" +
                    "|_ ", "2")]
        [InlineData(" _ " +
                    " _|" +
                    " _|", "3")]
        [InlineData("   " +
                    "|_|" +
                    "  |", "4")]
        [InlineData(" _ " +
                    "|_ " +
                    " _|", "5")]
        [InlineData(" _ " +
                    "|_ " +
                    "|_|", "6")]
        [InlineData(" _ " +
                    "  |" +
                    "  |", "7")]
        [InlineData(" _ " +
                    "|_|" +
                    "|_|", "8")]
        [InlineData(" _ " +
                    "|_|" +
                    " _|", "9")]
        public void ParseSingleDigit_ValidDigitFormat_ReturnsExpectedNumber(string digitFormatted, string expectedDigit)
        {
            IAccountNumberParser parser = new DigitalAccountNumberParser(Mock.Of<IAccountNumberValidator>());
            
            var parseResult = parser.ParseDigit(digitFormatted);

            Assert.Equal(expectedDigit, parseResult.DigitsParsed);
        }

        [Theory]
        [InlineData(" _ " +
                    "|  " +
                    "|_|", "?")]
        [InlineData("   " +
                    " _|" +
                    "  |", "?")]
        [InlineData(" _ " +
                    " _|" +
                    " _ ", "?")]
        public void ParseSingleDigit_IllegibleDigit_ReturnsIllegibleStatus(string digitFormatted, string expectedDigit)
        {
            IAccountNumberParser parser = new DigitalAccountNumberParser(Mock.Of<IAccountNumberValidator>());
            
            var parseResult = parser.ParseDigit(digitFormatted);

            Assert.Equal(expectedDigit, parseResult.DigitsParsed);
        }

        [Theory]
        [InlineData("    _  _     _  _  _  _  _ \r\n" +
                    "  | _| _||_||_ |_   ||_||_|\r\n" +
                    "  ||_  _|  | _||_|  ||_| _|", "123456789")]
        [InlineData(" _  _  _  _     _  _     _ \r\n" +
                    "|_   ||_||_|  | _| _||_||_ \r\n" +
                    "|_|  ||_| _|  ||_  _|  | _|", "678912345")]
        [InlineData(" _     _  _     _  _  _  _ \r\n" +
                    "| |  | _| _||_||_ |_   ||_|\r\n" +
                    "|_|  ||_  _|  | _||_|  ||_|", "012345678")]
        public void ParseAccountNumber_ValidDigitFormat_ReturnsExpectedNumber(string accountNumberFormatted, string expectedAccountNumber)
        {
            IAccountNumberParser parser = new DigitalAccountNumberParser(GetHappyValidator());

            var parseResult = parser.ParseAccountNumber(accountNumberFormatted);

            Assert.Equal(expectedAccountNumber, parseResult.DigitsParsed);
        }

        [Theory]
        [InlineData("    _  _     _  _  _  _  _ \r\n" +
                    "  ||_  _|  | _||_|  ||_| _|")]
        [InlineData("|_|  ||_| _|  ||_  _|  | _|")]
        [InlineData(" _     _  _     _  _  _  _ \r\n" +
                    "| |  | _| _||_||_ |_   ||_|\r\n" +
                    "|_|  ||_  _|  | _||_|  ||_|\r\n")]
        public void ParseAccountNumber_IncorrectNumberOfLines_ThrowsException(string accountNumberFormatted)
        {
            IAccountNumberParser parser = new DigitalAccountNumberParser(GetHappyValidator());

            var action = new Action(() => parser.ParseAccountNumber(accountNumberFormatted));

            var ex = Assert.Throws<ArgumentException>(action);
            Assert.Contains("Incorrect line count", ex.Message);
        }

        [Theory]
        [InlineData("    _  _     _  _   _  _  _ \r\n" +
                    "  | _| _||_||_ |_    ||_||_|\r\n" +
                    "  ||_  _|  | _||_|   ||_| _|")]
        [InlineData(" _  _  _  _     _  _     _ \r\n" +
                    "|_   ||_||_|  | _|  _||_||_ \r\n" +
                    "|_|  ||_| _|  ||_  _|  | _|")]
        [InlineData(" _     _  _     _  _  _  _ \r\n" +
                    "| |  | _| _||_||_ |_   ||_|\r\n" +
                    "||_  _|  | _||_|  ||_|")]
        public void ParseAccountNumber_IncorrectLength_ThrowsException(string accountNumberFormatted)
        {
            IAccountNumberParser parser = new DigitalAccountNumberParser(Mock.Of<IAccountNumberValidator>());

            var action = new Action(() => parser.ParseAccountNumber(accountNumberFormatted));

            var ex = Assert.Throws<ArgumentException>(action);
            Assert.Contains("Incorrect line length", ex.Message);
        }

        [Theory]
        [InlineData("    _  _     _  _  _  _  _ \r\n" +
                    "  | _| _||_||_ |_   ||_||_|\r\n" +
                    "  ||_  _|  | _||_|  ||_| _|", "123456789", ParseStatus.OK)]
        [InlineData(" _  _  _  _     _  _     _ \r\n" +
                    "|_   ||_||_|  | _| _||_||_|\r\n" +
                    "|_|  ||_| _|  ||_  _|  ||_|", "678912348", ParseStatus.ERR)]
        [InlineData(" _     _  _     _  _  _  _ \r\n" +
                    "| |  | _| _||_||_ |    ||_|\r\n" +
                    "|_|  ||_  _|  |  ||_|  ||_|", "01234??78", ParseStatus.ILL)]
        public void ParseAccountNumber_VariousNumbers_ReturnsExpectedStatus(string accountNumberFormatted, string expectedAccountNumber, ParseStatus expectedParseStatus)
        {
            IAccountNumberParser parser = new DigitalAccountNumberParser(new AccountNumberValidator());

            var parseResult = parser.ParseAccountNumber(accountNumberFormatted);

            Assert.Equal(expectedAccountNumber, parseResult.DigitsParsed);
            Assert.Equal(expectedParseStatus, parseResult.Status);
        }

        private static IAccountNumberValidator GetHappyValidator()
        {
            var validatorMock = new Mock<IAccountNumberValidator>();
            validatorMock.Setup(x => x.Validate(It.IsAny<string>())).Returns(new ValidationResult(string.Empty));
            return validatorMock.Object;
        }
    }
}
