using System;
using System.Collections.Generic;
using System.Linq;
using BankOCR.Console.Interfaces;
using BankOCR.Console.Models;

namespace BankOCR.Console.Validation
{
    public class AccountNumberValidator : IAccountNumberValidator
    {
        public const int AccountNumberLength = 9;

        public const string IncorrectLengthPrefix = "Incorrect length";
        public const string ChecksumErrorPrefix = "Checksum error";

        public ValidationResult Validate(string accountNumber)
        {
            var errors = new List<string>();

            if (accountNumber.Length != AccountNumberLength)
                errors.Add($"{IncorrectLengthPrefix}: '{accountNumber}' is {accountNumber.Length} character(s) but should be {AccountNumberLength} characters.");

            if (!errors.Any())
            {
                var checkSum = 0;
                for (var i = 1; i <= AccountNumberLength; ++i)
                {
                    int digit = accountNumber[AccountNumberLength - i] - '0';
                    checkSum += digit * i;
                }

                if (checkSum % 11 != 0)
                    errors.Add(ChecksumErrorPrefix);
            }

            return new ValidationResult(accountNumber, errors);
        }
    }
}