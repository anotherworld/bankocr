using BankOCR.Console.Models;

namespace BankOCR.Console.Interfaces
{
    public interface IAccountNumberParser
    {
        DigitsParseResult ParseDigit(string digitFormat);
        DigitsParseResult ParseAccountNumber(string accountNumberFormatted);
    }
}