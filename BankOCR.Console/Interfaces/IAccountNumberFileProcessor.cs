using System.Collections.Generic;
using BankOCR.Console.Models;
using BankOCR.Console.Services;

namespace BankOCR.Console.Interfaces
{
    public interface IAccountNumberFileProcessor
    {
        IEnumerable<DigitsParseResult> ProcessLines(string[] lines);
    }
}