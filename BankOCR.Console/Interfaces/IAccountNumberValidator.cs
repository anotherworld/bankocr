using System.Collections.Generic;
using BankOCR.Console.Models;

namespace BankOCR.Console.Interfaces
{
    public interface IAccountNumberValidator
    {
        ValidationResult Validate(string accountNumber);
    }
}