namespace BankOCR.Console.Models
{
    public class DigitsParseResult
    {
        public string DigitsFormatted { get; private set; }
        public string DigitsParsed { get; private set; }
        public ParseStatus Status { get; private set; }

        public DigitsParseResult(string digitsFormatted, string digitsParsed, ParseStatus status)
        {
            DigitsFormatted = digitsFormatted;
            DigitsParsed = digitsParsed;
            Status = status;
        }
    }
}