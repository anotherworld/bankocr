using System;
using System.Collections.Generic;

namespace BankOCR.Console.Models
{
    public class ValidationResult
    {
        public string AccountNumber { get; set; }
        public List<string> Errors { get; set; }

        public ValidationResult(string accountNumber, List<string> errors = null)
        {
            AccountNumber = accountNumber;
            Errors = errors ?? new List<string>();
        }
    }
}