﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Xml.Schema;
using BankOCR.Console.Interfaces;
using BankOCR.Console.Services;
using BankOCR.Console.Validation;

namespace BankOCR.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            ValidateArguments(args);
            
            if (args[0].Equals("parse", StringComparison.OrdinalIgnoreCase))
                ParseAccountNumbers(args[1]);
            else if (args[0].Equals("validate", StringComparison.OrdinalIgnoreCase))
                ValidateAccountNumbers(args[1]);
            
            if (Debugger.IsAttached)
                System.Console.ReadKey();
        }

        private static void ValidateArguments(string[] args)
        {
            if (args.Length != 2 || !new[] {"parse", "validate"}.Contains(args[0], StringComparer.OrdinalIgnoreCase))
            {
                System.Console.WriteLine("Syntax: BankOCR.Console <parse|validate> <input filename>");
                System.Console.WriteLine(" - parse attempts to parse the formatted account numbers within the file");
                System.Console.WriteLine(" - validate validates a list of (already parsed) account numbers within a file");
                Environment.Exit(1);
            }
        }

        private static void ParseAccountNumbers(string filename)
        {
            var parser = new DigitalAccountNumberParser(new AccountNumberValidator());
            var processor = new AccountNumberBatchProcessor(parser);
            var results = processor.ProcessLines(File.ReadAllLines(filename));

            foreach (var result in results)
            {
                System.Console.WriteLine(result.DigitsFormatted);
                System.Console.WriteLine("\r\n=> " + result.DigitsParsed + " " + result.Status);
            }
        }

        private static void ValidateAccountNumbers(string filename)
        {
            var validator = new AccountNumberValidator();
            var accountNumbers = File.ReadAllLines(filename);

            var validationResults = accountNumbers.Select(x => validator.Validate(x)).ToList();

            System.Console.WriteLine("Valid:");
            foreach (var result in validationResults.Where(x => !x.Errors.Any()))
                System.Console.WriteLine(result.AccountNumber);

            System.Console.WriteLine();

            System.Console.WriteLine("Invalid:");
            foreach (var result in validationResults.Where(x => x.Errors.Any()))
                System.Console.WriteLine(result.AccountNumber);
        }
    }
}
