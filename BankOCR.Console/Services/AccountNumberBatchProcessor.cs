using System;
using System.Collections.Generic;
using System.Linq;
using BankOCR.Console.Interfaces;
using BankOCR.Console.Models;

namespace BankOCR.Console.Services
{
    public class AccountNumberBatchProcessor : IAccountNumberFileProcessor
    {
        public const int EntryLineCount = 4;

        private readonly IAccountNumberParser _accountNumberParser;

        public AccountNumberBatchProcessor(IAccountNumberParser accountNumberParser)
        {
            _accountNumberParser = accountNumberParser;
        }

        public IEnumerable<DigitsParseResult> ProcessLines(string[] lines)
        {
            if (lines.Length % EntryLineCount != 0)
                throw new ArgumentException(
                    $"Incorrect line count: number of lines should be a multiple of {EntryLineCount}");

            var results = new List<DigitsParseResult>();
            for (var line = 0; line < lines.Length; line += EntryLineCount)
            {
                var entry = lines.Skip(line).Take(EntryLineCount).ToArray();
                var accountNumberFormatted = ProcessEntry(entry);
                var parseResult = _accountNumberParser.ParseAccountNumber(accountNumberFormatted);
                results.Add(parseResult);
            }

            return results;
        }

        private string ProcessEntry(string[] entry)
        {
            if (entry[EntryLineCount - 1].Length > 0)
                throw new FormatException("Invalid format: Lines contains an invalid entry - non-empty last line");

            var accountNumberFormatted = string.Join("\r\n", entry.Take(EntryLineCount - 1));

            return accountNumberFormatted;
        }
    }
}