using System;
using System.Collections.Generic;
using System.Linq;
using BankOCR.Console.Interfaces;
using BankOCR.Console.Models;
using BankOCR.Console.Validation;

namespace BankOCR.Console.Services
{
    public class DigitalAccountNumberParser : IAccountNumberParser
    {
        public const string AllDigitsFormat =
            " _     _  _     _  _  _  _  _ \r\n" +
            "| |  | _| _||_||_ |_   ||_||_|\r\n" +
            "|_|  ||_  _|  | _||_|  ||_| _|";

        public const int DigitWidth = 3;
        public const int DigitHeight = 3;
        public const int FormattedAccountNumberLength = 27;
        public const int AccountNumberLength = 9;

        public const char IllegibleCharacterSymbol = '?';

        private readonly string[] _digitFormat;
        private readonly IAccountNumberValidator _accountNumberValidator;

        public DigitalAccountNumberParser(IAccountNumberValidator accountNumberValidator)
        {
            _accountNumberValidator = accountNumberValidator;

            _digitFormat = GetIndividualDigits(AllDigitsFormat, 10);
        }

        private string[] GetIndividualDigits(string digitsFormatted, int numberOfDigits)
        {
            var digitFormat = new string[numberOfDigits];

            var formatLines = digitsFormatted.Split("\r\n");
            for (var digit = 0; digit < numberOfDigits; ++digit)
                digitFormat[digit] = string.Join("", 
                    formatLines
                        .Select(x => x.Substring(digit * DigitWidth, DigitWidth)));

            return digitFormat;
        }

        public DigitsParseResult ParseDigit(string digitFormatted)
        {
            for (var digit = 0; digit <= 9; ++digit)
                if (digitFormatted == _digitFormat[digit])
                    return new DigitsParseResult(digitFormatted, digit.ToString(), ParseStatus.OK);

            return new DigitsParseResult(digitFormatted, IllegibleCharacterSymbol.ToString(), ParseStatus.ILL);
        }

        public DigitsParseResult ParseAccountNumber(string accountNumberFormatted)
        {
            var accountNumberLines = accountNumberFormatted.Split("\r\n");
            ValidateFormattedAccountNumber(accountNumberLines);

            var parsedDigits = GetParsedDigits(accountNumberFormatted);
            var accountNumber = string.Join(string.Empty, parsedDigits.Select(x => x.DigitsParsed));
            var parseStatus = GetParseStatus(accountNumber, parsedDigits);

            return new DigitsParseResult(accountNumberFormatted, accountNumber, parseStatus);
        }

        private List<DigitsParseResult> GetParsedDigits(string accountNumberFormatted)
        {
            var accountNumberDigitsFormatted = GetIndividualDigits(accountNumberFormatted, AccountNumberLength);
            var parsedDigits = new List<DigitsParseResult>();
            foreach (var accountNumberDigitFormatted in accountNumberDigitsFormatted)
                parsedDigits.Add(ParseDigit(accountNumberDigitFormatted));
            return parsedDigits;
        }

        private ParseStatus GetParseStatus(string accountNumber, List<DigitsParseResult> parsedDigits)
        {
            var validationResult = _accountNumberValidator.Validate(accountNumber);

            var anyIllegibleDigits = parsedDigits.Any(x => x.Status == ParseStatus.ILL);
            var anyChecksumErrors = validationResult.Errors.Any(x => x.StartsWith(AccountNumberValidator.ChecksumErrorPrefix));

            var parseStatus = anyIllegibleDigits 
                    ? ParseStatus.ILL 
                    : anyChecksumErrors
                        ? ParseStatus.ERR
                        : ParseStatus.OK;
            
            return parseStatus;
        }

        private static void ValidateFormattedAccountNumber(string[] accountNumberLines)
        {
            if (accountNumberLines.Length != DigitHeight)
                throw new ArgumentException($"Incorrect line count: AccountNumberFormatted should have {DigitHeight} lines");

            if (accountNumberLines.Any(x => x.Length != FormattedAccountNumberLength))
            {
                var accountNumberLinesAsString = string.Join("\r\n", accountNumberLines.Select(x => "'" + x + "'"));
                throw new ArgumentException(
                    $"Incorrect line length: All lines in AccountNumberFormatted should be {FormattedAccountNumberLength} characters.\r\n{accountNumberLinesAsString}");
            }
        }
    }
}